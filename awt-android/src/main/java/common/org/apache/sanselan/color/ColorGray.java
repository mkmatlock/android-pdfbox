package org.apache.sanselan.color;

public class ColorGray {
	public final double I;
	
	public ColorGray(double I){
		this.I = I;
	}
	

	public final String toString()
	{
		return "{GRAY: " + I + "}";
	}
}

