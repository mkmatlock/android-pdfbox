/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.sanselan.icc;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.harmony.awt.internal.nls.Messages;
import org.apache.sanselan.ImageReadException;

import com.google.code.appengine.awt.color.ColorSpace;
import com.google.code.appengine.awt.color.ProfileDataException;

public class IccProfileInfo implements IccConstants
{


    public static final int CLASS_INPUT = 0;

    public static final int CLASS_DISPLAY = 1;

    public static final int CLASS_OUTPUT = 2;

    public static final int CLASS_DEVICELINK = 3;

    public static final int CLASS_COLORSPACECONVERSION = 4;

    public static final int CLASS_ABSTRACT = 5;

    public static final int CLASS_NAMEDCOLOR = 6;

    public static final int icSigXYZData = 1482250784;

    public static final int icSigLabData = 1281450528;

    public static final int icSigLuvData = 1282766368;

    public static final int icSigYCbCrData = 1497588338;

    public static final int icSigYxyData = 1501067552;

    public static final int icSigRgbData = 1380401696;

    public static final int icSigGrayData = 1196573017;

    public static final int icSigHsvData = 1213421088;

    public static final int icSigHlsData = 1212961568;

    public static final int icSigCmykData = 1129142603;

    public static final int icSigCmyData = 1129142560;

    public static final int icSigSpace2CLR = 843271250;

    public static final int icSigSpace3CLR = 860048466;

    public static final int icSigSpace4CLR = 876825682;

    public static final int icSigSpace5CLR = 893602898;

    public static final int icSigSpace6CLR = 910380114;

    public static final int icSigSpace7CLR = 927157330;

    public static final int icSigSpace8CLR = 943934546;

    public static final int icSigSpace9CLR = 960711762;

    public static final int icSigSpaceACLR = 1094929490;

    public static final int icSigSpaceBCLR = 1111706706;

    public static final int icSigSpaceCCLR = 1128483922;

    public static final int icSigSpaceDCLR = 1145261138;

    public static final int icSigSpaceECLR = 1162038354;

    public static final int icSigSpaceFCLR = 1178815570;

    public static final int icSigInputClass = 1935896178;

    public static final int icSigDisplayClass = 1835955314;

    public static final int icSigOutputClass = 1886549106;

    public static final int icSigLinkClass = 1818848875;

    public static final int icSigAbstractClass = 1633842036;

    public static final int icSigColorantOrderTag = 1668051567;

    public static final int icSigColorantTableTag = 1668051572;

    public static final int icSigColorSpaceClass = 1936744803;

    public static final int icSigNamedColorClass = 1852662636;

    public static final int icPerceptual = 0;

    public static final int icRelativeColorimetric = 1;

    public static final int icSaturation = 2;

    public static final int icAbsoluteColorimetric = 3;

    public static final int icSigHead = 1751474532;

    public static final int icSigAToB0Tag = 1093812784;

    public static final int icSigAToB1Tag = 1093812785;

    public static final int icSigAToB2Tag = 1093812786;

    public static final int icSigBlueColorantTag = 1649957210;

    public static final int icSigBlueMatrixColumnTag = 1649957210;

    public static final int icSigBlueTRCTag = 1649693251;

    public static final int icSigBToA0Tag = 1110589744;

    public static final int icSigBToA1Tag = 1110589745;

    public static final int icSigBToA2Tag = 1110589746;

    public static final int icSigCalibrationDateTimeTag = 1667329140;

    public static final int icSigCharTargetTag = 1952543335;

    public static final int icSigCopyrightTag = 1668313716;

    public static final int icSigCrdInfoTag = 1668441193;

    public static final int icSigDeviceMfgDescTag = 1684893284;

    public static final int icSigDeviceModelDescTag = 1684890724;

    public static final int icSigDeviceSettingsTag = 1684371059;

    public static final int icSigGamutTag = 1734438260;

    public static final int icSigGrayTRCTag = 1800688195;

    public static final int icSigGreenColorantTag = 1733843290;

    public static final int icSigGreenMatrixColumnTag = 1733843290;

    public static final int icSigGreenTRCTag = 1733579331;

    public static final int icSigLuminanceTag = 1819635049;

    public static final int icSigMeasurementTag = 1835360627;

    public static final int icSigMediaBlackPointTag = 1651208308;

    public static final int icSigMediaWhitePointTag = 2004119668;

    public static final int icSigNamedColor2Tag = 1852009522;

    public static final int icSigOutputResponseTag = 1919251312;

    public static final int icSigPreview0Tag = 1886545200;

    public static final int icSigPreview1Tag = 1886545201;

    public static final int icSigPreview2Tag = 1886545202;

    public static final int icSigProfileDescriptionTag = 1684370275;

    public static final int icSigProfileSequenceDescTag = 1886610801;

    public static final int icSigPs2CRD0Tag = 1886610480;

    public static final int icSigPs2CRD1Tag = 1886610481;

    public static final int icSigPs2CRD2Tag = 1886610482;

    public static final int icSigPs2CRD3Tag = 1886610483;

    public static final int icSigPs2CSATag = 1886597747;

    public static final int icSigPs2RenderingIntentTag = 1886597737;

    public static final int icSigRedColorantTag = 1918392666;

    public static final int icSigRedMatrixColumnTag = 1918392666;

    public static final int icSigRedTRCTag = 1918128707;

    public static final int icSigScreeningDescTag = 1935897188;

    public static final int icSigScreeningTag = 1935897198;

    public static final int icSigTechnologyTag = 1952801640;

    public static final int icSigUcrBgTag = 1650877472;

    public static final int icSigViewingCondDescTag = 1987405156;

    public static final int icSigViewingConditionsTag = 1986618743;

    public static final int icSigChromaticAdaptationTag = 1667785060;

    public static final int icSigChromaticityTag = 1667789421;

    public static final int icHdrSize = 0;

    public static final int icHdrCmmId = 4;

    public static final int icHdrVersion = 8;

    public static final int icHdrDeviceClass = 12;

    public static final int icHdrColorSpace = 16;

    public static final int icHdrPcs = 20;

    public static final int icHdrDate = 24;

    public static final int icHdrMagic = 36;

    public static final int icHdrPlatform = 40;

    public static final int icHdrProfileID = 84;

    public static final int icHdrFlags = 44;

    public static final int icHdrManufacturer = 48;

    public static final int icHdrModel = 52;

    public static final int icHdrAttributes = 56;

    public static final int icHdrRenderingIntent = 64;

    public static final int icHdrIlluminant = 68;

    public static final int icHdrCreator = 80;

    public static final int icICCAbsoluteColorimetric = 3;

    public static final int icMediaRelativeColorimetric = 1;

    public static final int icTagType = 0;

    public static final int icTagReserved = 4;

    public static final int icCurveCount = 8;

    public static final int icCurveData = 12;

    public static final int icXYZNumberX = 8;

	
	public final byte data[];
	public final int profileSize;
	public final int CMMTypeSignature;
	public final int profileVersion;
	public final int profileDeviceClassSignature;
	public final int colorSpace;
	public final int profileConnectionSpace;
	public final int profileFileSignature;
	public final int primaryPlatformSignature;
	public final int variousFlags;
	public final int deviceManufacturer;
	public final int deviceModel;
	public final int renderingIntent;
	public final int profileCreatorSignature;
	public final byte profileID[];
	public final IccTag tags[];

	public IccProfileInfo(byte data[], int ProfileSize, int CMMTypeSignature,
			int ProfileVersion, int ProfileDeviceClassSignature,
			int ColorSpace, int ProfileConnectionSpace,
			int ProfileFileSignature, int PrimaryPlatformSignature,
			int VariousFlags, int DeviceManufacturer, int DeviceModel,
			int RenderingIntent, int ProfileCreatorSignature, byte ProfileID[],
			IccTag tags[])
	{
		this.data = data;

		this.profileSize = ProfileSize;
		this.CMMTypeSignature = CMMTypeSignature;
		this.profileVersion = ProfileVersion;
		this.profileDeviceClassSignature = ProfileDeviceClassSignature;
		this.colorSpace = ColorSpace;
		this.profileConnectionSpace = ProfileConnectionSpace;
		this.profileFileSignature = ProfileFileSignature;
		this.primaryPlatformSignature = PrimaryPlatformSignature;
		this.variousFlags = VariousFlags;
		this.deviceManufacturer = DeviceManufacturer;
		this.deviceModel = DeviceModel;
		this.renderingIntent = RenderingIntent;
		this.profileCreatorSignature = ProfileCreatorSignature;
		this.profileID = ProfileID;

		this.tags = tags;
	}

	public boolean issRGB()
	{
		boolean result = ((deviceManufacturer == IEC) && (deviceModel == sRGB));
		return result;
	}

	private void printCharQuad(PrintWriter pw, String msg, int i)
	{
		pw.println(msg + ": '" + (char) (0xff & (i >> 24))
				+ (char) (0xff & (i >> 16)) + (char) (0xff & (i >> 8))
				+ (char) (0xff & (i >> 0)) + "'");
	}

	public void dump(String prefix) throws IOException
	{
		System.out.print(toString());
	}

	public String toString()
	{
		try
		{
			return toString("");
		}
		catch (Exception e)
		{
			return "IccProfileInfo: Error";
		}
	}

	public String toString(String prefix) throws ImageReadException,
			IOException
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);

		//		StringBuffer result = new StringBuffer();
		pw.println(prefix + ": " + "data length: " + data.length);

		printCharQuad(pw, prefix + ": " + "ProfileDeviceClassSignature",
				profileDeviceClassSignature);

		printCharQuad(pw, prefix + ": " + "CMMTypeSignature", CMMTypeSignature);

		printCharQuad(pw, prefix + ": " + "ProfileDeviceClassSignature",
				profileDeviceClassSignature);
		printCharQuad(pw, prefix + ": " + "ColorSpace", colorSpace);
		printCharQuad(pw, prefix + ": " + "ProfileConnectionSpace",
				profileConnectionSpace);

		printCharQuad(pw, prefix + ": " + "ProfileFileSignature",
				profileFileSignature);

		printCharQuad(pw, prefix + ": " + "PrimaryPlatformSignature",
				primaryPlatformSignature);

		printCharQuad(pw, prefix + ": " + "ProfileFileSignature",
				profileFileSignature);

		printCharQuad(pw, prefix + ": " + "DeviceManufacturer",
				deviceManufacturer);

		printCharQuad(pw, prefix + ": " + "DeviceModel", deviceModel);

		printCharQuad(pw, prefix + ": " + "RenderingIntent", renderingIntent);

		printCharQuad(pw, prefix + ": " + "ProfileCreatorSignature",
				profileCreatorSignature);

		for (int i = 0; i < tags.length; i++)
		{
			IccTag tag = tags[i];
			tag.dump(pw, "\t" + i + ": ");
		}

		pw.println(prefix + ": " + "issRGB: " + issRGB());
		pw.flush();

		return sw.getBuffer().toString();
	}

    public int getColorSpaceType() {
        return csFromSignature(colorSpace);
    }

    public int getVersion() {
        return profileVersion;
    }

    public int getNumComponents() {
        switch (colorSpace) {
            // The most common cases go first to increase speed
            case icSigRgbData:
            case icSigXYZData:
            case icSigLabData:
                return 3;
            case icSigCmykData:
                return 4;
                // Then all other
            case icSigGrayData:
                return 1;
            case icSigSpace2CLR:
                return 2;
            case icSigYCbCrData:
            case icSigLuvData:
            case icSigYxyData:
            case icSigHlsData:
            case icSigHsvData:
            case icSigCmyData:
            case icSigSpace3CLR:
                return 3;
            case icSigSpace4CLR:
                return 4;
            case icSigSpace5CLR:
                return 5;
            case icSigSpace6CLR:
                return 6;
            case icSigSpace7CLR:
                return 7;
            case icSigSpace8CLR:
                return 8;
            case icSigSpace9CLR:
                return 9;
            case icSigSpaceACLR:
                return 10;
            case icSigSpaceBCLR:
                return 11;
            case icSigSpaceCCLR:
                return 12;
            case icSigSpaceDCLR:
                return 13;
            case icSigSpaceECLR:
                return 14;
            case icSigSpaceFCLR:
                return 15;
            default:
        }

        // awt.160=Color space doesn't comply with ICC specification
        throw new ProfileDataException(Messages.getString("awt.160") //$NON-NLS-1$
        );
    }


    public int getProfileClass() {
        switch (profileDeviceClassSignature) {
            case icSigColorSpaceClass:
                return CLASS_COLORSPACECONVERSION;
            case icSigDisplayClass:
                return CLASS_DISPLAY;
            case icSigOutputClass:
                return CLASS_OUTPUT;
            case icSigInputClass:
                return CLASS_INPUT;
            case icSigLinkClass:
                return CLASS_DEVICELINK;
            case icSigAbstractClass:
                return CLASS_ABSTRACT;
            case icSigNamedColorClass:
                return CLASS_NAMEDCOLOR;
            default:
        }

        // Not an ICC profile class
        // awt.15F=Profile class does not comply with ICC specification
        throw new IllegalArgumentException(Messages.getString("awt.15F")); //$NON-NLS-1$
        
    }

    /**
     * Converts ICC color space signature to the java predefined
     * color space type
     * @param signature
     * @return
     */
    private int csFromSignature(int signature) {
        switch (signature) {
            case icSigRgbData:
                return ColorSpace.TYPE_RGB;
            case icSigXYZData:
                return ColorSpace.TYPE_XYZ;
            case icSigCmykData:
                return ColorSpace.TYPE_CMYK;
            case icSigLabData:
                return ColorSpace.TYPE_Lab;
            case icSigGrayData:
                return ColorSpace.TYPE_GRAY;
            case icSigHlsData:
                return ColorSpace.TYPE_HLS;
            case icSigLuvData:
                return ColorSpace.TYPE_Luv;
            case icSigYCbCrData:
                return ColorSpace.TYPE_YCbCr;
            case icSigYxyData:
                return ColorSpace.TYPE_Yxy;
            case icSigHsvData:
                return ColorSpace.TYPE_HSV;
            case icSigCmyData:
                return ColorSpace.TYPE_CMY;
            case icSigSpace2CLR:
                return ColorSpace.TYPE_2CLR;
            case icSigSpace3CLR:
                return ColorSpace.TYPE_3CLR;
            case icSigSpace4CLR:
                return ColorSpace.TYPE_4CLR;
            case icSigSpace5CLR:
                return ColorSpace.TYPE_5CLR;
            case icSigSpace6CLR:
                return ColorSpace.TYPE_6CLR;
            case icSigSpace7CLR:
                return ColorSpace.TYPE_7CLR;
            case icSigSpace8CLR:
                return ColorSpace.TYPE_8CLR;
            case icSigSpace9CLR:
                return ColorSpace.TYPE_9CLR;
            case icSigSpaceACLR:
                return ColorSpace.TYPE_ACLR;
            case icSigSpaceBCLR:
                return ColorSpace.TYPE_BCLR;
            case icSigSpaceCCLR:
                return ColorSpace.TYPE_CCLR;
            case icSigSpaceDCLR:
                return ColorSpace.TYPE_DCLR;
            case icSigSpaceECLR:
                return ColorSpace.TYPE_ECLR;
            case icSigSpaceFCLR:
                return ColorSpace.TYPE_FCLR;
            default:
        }

        // awt.165=Color space doesn't comply with ICC specification
        throw new IllegalArgumentException (Messages.getString("awt.165")); //$NON-NLS-1$
    }
}