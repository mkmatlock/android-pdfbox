/*
 *  Licensed to the Apache Software Foundation (ASF) under one or more
 *  contributor license agreements.  See the NOTICE file distributed with
 *  this work for additional information regarding copyright ownership.
 *  The ASF licenses this file to You under the Apache License, Version 2.0
 *  (the "License"); you may not use this file except in compliance with
 *  the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.google.code.appengine.awt;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.Arrays;

import junit.framework.Assert;

import com.google.code.appengine.awt.geom.AffineTransform;
import com.google.code.appengine.awt.geom.GeneralPath;

public abstract class Tools {

    static String typeName[] = {"move", "line", "quad", "cubic", "close"};

    static int findString(String buf[], String value) {
        for(int i = 0; i < buf.length; i++) {
            if (buf[i].equals(value)) {
                return i;
            }
        }
        Assert.fail("Unknown value " + value);
        return -1;
    }
    
    public static String getClasstPath(Class clazz) {
        String name = clazz.getName();
        name = name.substring(0, name.lastIndexOf('.'));

        return name.replace('.', '/') + '/';
    }
    
    public static class Shape {

        static int pointCount[] = {
                2, // MOVE
                2, // LINE
                4, // QUAD
                6, // CUBIC
                0  // CLOSE
            };

        static final double IMAGE_REL_BORDER = 0.1;
        static final double IMAGE_MIN_BORDER = 10.0;
        static final double IMAGE_MAX_BORDER = 100.0;
        static final Color backColor = Color.white;

        public static void save(com.google.code.appengine.awt.Shape s, String fileName) {
            try {
                FileWriter f = new FileWriter(fileName);
                com.google.code.appengine.awt.geom.PathIterator p = s.getPathIterator(null);
                double coords[] = new double[6];
                while(!p.isDone()) {
                    int type = p.currentSegment(coords);
                    f.write(typeName[type] + getCoords(coords, pointCount[type]) + "\n");
                    p.next();
                }
                f.close();
            } catch (IOException e) {
                Assert.fail("Can''t write to file " + fileName);
            }
        }

        public static com.google.code.appengine.awt.Shape load(String fileName) {
            GeneralPath s = null;
            try {
                FileReader f = new FileReader(fileName);
                s = new GeneralPath();
                StreamTokenizer t = new StreamTokenizer(f);
                int count = 0;
                int type = 0;
                float coords[] = new float[6];
                while(t.nextToken() != StreamTokenizer.TT_EOF) {
                    switch(t.ttype) {
                    case StreamTokenizer.TT_EOL:
                        break;
                    case StreamTokenizer.TT_WORD:
                        type = findString(typeName, t.sval);
                        if (type == com.google.code.appengine.awt.geom.PathIterator.SEG_CLOSE) {
                            s.closePath();
                        }
                        break;
                    case StreamTokenizer.TT_NUMBER:
                        coords[count++] = (float)t.nval;
                        if (count == pointCount[type]) {
                            count = 0;
                            switch(type) {
                            case com.google.code.appengine.awt.geom.PathIterator.SEG_MOVETO:
                                s.moveTo(coords[0], coords[1]);
                                break;
                            case com.google.code.appengine.awt.geom.PathIterator.SEG_LINETO:
                                s.lineTo(coords[0], coords[1]);
                                break;
                            case com.google.code.appengine.awt.geom.PathIterator.SEG_QUADTO:
                                s.quadTo(coords[0], coords[1], coords[2], coords[3]);
                                break;
                            case com.google.code.appengine.awt.geom.PathIterator.SEG_CUBICTO:
                                s.curveTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
                                break;
                            }
                        }
                        break;
                    }
                }
                f.close();
            } catch (IOException e) {
                Assert.fail("Can''t read file " + fileName);
            }
            return s;
        }

        static String getCoords(double coords[], int count) {
            String s = "";
            for(int i = 0; i < count; i++) {
                s = s + " " + coords[i];
            }
            return s;
        }


        public static boolean equals(com.google.code.appengine.awt.Shape s1, com.google.code.appengine.awt.Shape s2, double delta) {
            return PathIterator.equals(s1.getPathIterator(null), s2.getPathIterator(null), delta);
        }

        public static boolean equals(com.google.code.appengine.awt.Shape s1, com.google.code.appengine.awt.Shape s2, float delta) {
            return PathIterator.equals(s1.getPathIterator(null), s2.getPathIterator(null), delta);
        }

        public static com.google.code.appengine.awt.Shape scale(com.google.code.appengine.awt.Shape shape, double k) {
            com.google.code.appengine.awt.geom.PathIterator path = shape.getPathIterator(AffineTransform.getScaleInstance(k, k));
            com.google.code.appengine.awt.geom.GeneralPath dst = new com.google.code.appengine.awt.geom.GeneralPath(path.getWindingRule());
            dst.append(path, false);
            return dst;
        }

        public static com.google.code.appengine.awt.Shape flip(com.google.code.appengine.awt.Shape shape) {
            com.google.code.appengine.awt.geom.PathIterator path = shape.getPathIterator(new AffineTransform(0, 1, 1, 0, 0, 0));
            com.google.code.appengine.awt.geom.GeneralPath dst = new com.google.code.appengine.awt.geom.GeneralPath(path.getWindingRule());
            dst.append(path, false);
            return dst;
        }

        public static String toString(com.google.code.appengine.awt.Shape shape) {
            return shape.getClass().getName() + "\n" + PathIterator.toString(shape.getPathIterator(null));
        }
    }

    public static class File {

        public static String changeExt(String file, String newExt) {
            int k = file.lastIndexOf('.');
            return file.substring(0, k) + newExt;
        }

        public static String extractFileName(String file) {
            int k;
            if ((k = file.lastIndexOf('/')) == -1) {
                if ((k = file.lastIndexOf('\\')) == -1) {
                    k = 1;
                }
            }
            return file.substring(k + 1);
        }

        public static String extractFileExt(String file) {
            int k = file.lastIndexOf('.');
            return file.substring(k + 1);
        }

    }

    public static class PathIterator {

        public static String equalsError = "";
        
        static boolean coordsEquals(double coords1[], double coords2[], int count, double delta) {
            for(int i = 0; i < count; i++) {
                if (Math.abs(coords1[i] - coords2[i]) > delta) {
                    return false;
                }
            }
            return true;
        }

        static boolean coordsEquals(float coords1[], float coords2[], int count, float delta) {
            for(int i = 0; i < count; i++) {
                if (Math.abs(coords1[i] - coords2[i]) > delta) {
                    return false;
                }
            }
            return true;
        }

        public static boolean equals(com.google.code.appengine.awt.geom.PathIterator p1, com.google.code.appengine.awt.geom.PathIterator p2, double delta) {
            equalsError = "";
            if (p1.getWindingRule() != p2.getWindingRule()) {
                equalsError = "WindingRule expected " + p1.getWindingRule() + " but was " + p2.getWindingRule();
                return false;
            }
            int count = 0;
            double coords1[] = new double[6];
            double coords2[] = new double[6];
            while(!p1.isDone() && !p2.isDone()) {
                int type1 = p1.currentSegment(coords1);
                int type2 = p2.currentSegment(coords2);
                if (type1 != type2 || !coordsEquals(coords1, coords2, Shape.pointCount[type1], delta)) {
                    equalsError = "Expected #" + count + " segment "+ typeName[type1] + Arrays.toString(coords1) + " but was " + typeName[type2] + Arrays.toString(coords2);
                    return false;
                }
                p1.next();
                p2.next();
                count++;
            }
            if (p1.isDone() != p2.isDone()) {
                equalsError = "Expected #" + count + " isDone " + p1.isDone() + " but was " + p2.isDone();
                return false;
            }
            return true;
        }

        public static boolean equals(com.google.code.appengine.awt.geom.PathIterator p1, com.google.code.appengine.awt.geom.PathIterator p2, float delta) {
            if (p1.getWindingRule() != p2.getWindingRule()) {
                return false;
            }
            float coords1[] = new float[6];
            float coords2[] = new float[6];
            while(!p1.isDone() && !p2.isDone()) {
                int type1 = p1.currentSegment(coords1);
                int type2 = p2.currentSegment(coords2);
                if (type1 != type2 || !coordsEquals(coords1, coords2, Shape.pointCount[type1], delta)) {
                    return false;
                }
                p1.next();
                p2.next();
            }
            if (p1.isDone() != p2.isDone()) {
                return false;
            }
            return true;
        }

        public static String toString(com.google.code.appengine.awt.geom.PathIterator path) {
            String out = "";
            float coords[] = new float[6];
            while(!path.isDone()) {
                switch(path.currentSegment(coords)) {
                case com.google.code.appengine.awt.geom.PathIterator.SEG_MOVETO:
                    out += "move(" + coords[0] + "," + coords[1] + ")\n";
                    break;
                case com.google.code.appengine.awt.geom.PathIterator.SEG_LINETO:
                    out += "line(" + coords[0] + "," + coords[1] + ")\n";
                    break;
                case com.google.code.appengine.awt.geom.PathIterator.SEG_QUADTO:
                    out += "quad(" + coords[0] + "," + coords[1] + "," + coords[2] + "," + coords[3] + ")\n";
                    break;
                case com.google.code.appengine.awt.geom.PathIterator.SEG_CUBICTO:
                    out += "cubic(" + coords[0] + "," + coords[1] + "," + coords[2] + "," + coords[3] + "," + coords[4] + "," + coords[5] + ")\n";
                    break;
                case com.google.code.appengine.awt.geom.PathIterator.SEG_CLOSE:
                    out += "close\n";
                    break;
                }
                path.next();
            }
            out += "done\n";
            return out;
        }
    }

    public static class MultiRectArea {

        public final static Color[] color = new Color[] {
                Color.blue,
                Color.green,
                Color.red,
                Color.yellow,
                Color.MAGENTA,
                Color.orange,
                Color.lightGray,
                Color.cyan,
                Color.pink
        };

        public final static Color[] colorBlue = new Color[] {
                new Color(0x3F),
                new Color(0x5F),
                new Color(0x7F),
                new Color(0x9F),
                new Color(0xBF),
                new Color(0xDF),
                new Color(0xFF)
        };

        public final static Color[] colorGreen = new Color[] {
                new Color(0x3F00),
                new Color(0x5F00),
                new Color(0x7F00),
                new Color(0x9F00),
                new Color(0xBF00),
                new Color(0xDF00),
                new Color(0xFF00)
        };

        static final int BORDER = 30;
        static final Color colorBack = Color.white;
    }

}
